package gr.arniotaki.dataversetest.base;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import gr.arniotaki.dataversetest.features.register.data.UserDao;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

@Database(entities = {UserDomain.class}, version = 1, exportSchema = false)
abstract public class MyDatabase extends RoomDatabase {
    public abstract UserDao userDao();


    static private MyDatabase INSTANCE;

    public static MyDatabase getDatabase(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(ctx.getApplicationContext(),
                    MyDatabase.class, "dataverse_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }


}
