package gr.arniotaki.dataversetest.features.login.presentation.ui;

import android.content.Context;
import android.widget.Toast;

import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.features.login.data.LoginInteractorImpl;
import gr.arniotaki.dataversetest.features.login.domain.LoginInteractor;
import gr.arniotaki.dataversetest.features.login.domain.LoginPresenter;
import gr.arniotaki.dataversetest.features.login.domain.LoginView;
import gr.arniotaki.dataversetest.features.register.data.MainInteractorImpl;
import gr.arniotaki.dataversetest.features.register.domain.MainInteractor;
import gr.arniotaki.dataversetest.features.register.domain.MainView;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

import static gr.arniotaki.dataversetest.base.MyApplication.isValidEmail;

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishListener {

    LoginView loginView;
    LoginInteractor interactor;


    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        interactor = new LoginInteractorImpl();
    }

    @Override
    public boolean checkFields(String password, String email, Context ctx) {
        //check empty fileds
        if (password.length() <= 0 || email.length()<=0) {
            Toast.makeText(ctx, ctx.getResources().getString(R.string.empty_fields_msg), Toast.LENGTH_LONG).show();
            return false;
        }else if (!isValidEmail(email)) {
            //check valid email
            Toast.makeText(ctx, ctx.getResources().getString(R.string.invalid_email_msg), Toast.LENGTH_LONG).show();
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void login(String password, String email, Context ctx) {
        this.interactor.login(this,password,email,ctx);
    }

    @Override
    public void onSuccess(UserDomain user) {
        this.loginView.userLoggedIn(true,null,user);
    }

    @Override
    public void onError(String message) {
        this.loginView.userLoggedIn(false,message,null);
    }
}
