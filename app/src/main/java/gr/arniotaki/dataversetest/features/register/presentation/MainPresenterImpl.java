package gr.arniotaki.dataversetest.features.register.presentation;

import android.content.Context;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.features.register.data.MainInteractorImpl;
import gr.arniotaki.dataversetest.features.register.domain.MainInteractor;
import gr.arniotaki.dataversetest.features.register.domain.MainPresenter;
import gr.arniotaki.dataversetest.features.register.domain.MainView;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

import static gr.arniotaki.dataversetest.base.MyApplication.isValidEmail;

public class MainPresenterImpl implements MainPresenter, MainInteractor.OnRegisterFinishListener {

    MainView mainView;
    MainInteractor interactor;


    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
        interactor = new MainInteractorImpl();
    }

    @Override
    //Returns true if everything is ok otherwise returns false.
    public boolean checkFields(String username, String password, String phone, String email, String repassword, String company, Context ctx) {
        //check empty fileds
        if (username.length()<=0 || password.length() <= 0 || phone.length()<=0 || email.length()<=0 || repassword.length()
        <=0 || company.length()<=0) {
            Toast.makeText(ctx, ctx.getResources().getString(R.string.empty_fields_msg), Toast.LENGTH_LONG).show();
            return false;
        }else if (!isValidEmail(email)) {
            //check valid email
            Toast.makeText(ctx, ctx.getResources().getString(R.string.invalid_email_msg), Toast.LENGTH_LONG).show();
            return false;
        }else if (!isPasswordValid(password,ctx)){
            return false;
        }else if (!password.equals(repassword)){
            Toast.makeText(ctx, ctx.getResources().getString(R.string.password_mismatch), Toast.LENGTH_LONG).show();
            return false;
        }else{
            return true;
        }
    }

    private static boolean isPasswordValid(String psw,Context ctx) {
        if (psw.length() <8 ) {
            //password must be at least 8 chars long
            Toast.makeText(ctx, ctx.getResources().getString(R.string.wrong_psw_length_msg), Toast.LENGTH_LONG).show();
            return false;
        }else if (!hasValidCharacters(psw)){
            Toast.makeText(ctx, ctx.getResources().getString(R.string.wrong_psw_char_msg), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private static boolean hasValidCharacters(String s) {
        //int counter =0;

        Pattern p = Pattern.compile("(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])");//replace this with your needs
        Matcher m = p.matcher(s);
        // boolean b = m.matches();

        boolean b = m.find();
        if (b == true)
            return true;
        else
            return false;
    }

    @Override
    public void registerUser(String username, String password, String phone, String email, String company, Context ctx) {
        this.interactor.register(this,username, password, phone, email, company, ctx);
    }

    @Override
    public void onSuccess(UserDomain user) {
        mainView.userRegistered(true,null,user);
    }

    @Override
    public void onError(String message) {
        mainView.userRegistered(false,message,null);
    }

}
