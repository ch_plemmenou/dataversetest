package gr.arniotaki.dataversetest.features.register.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

@Dao
public interface UserDao {

    @Insert
    long insertUser(UserDomain user);

//    @Query("SELECT * FROM user where password=:password and username=:username")
//    UserDomain getLoggedInUser(String password, String username);

    @Query("SELECT * FROM user")
    List<UserDomain> getUsers();

    @Query("SELECT * FROM user where email = :email")
    UserDomain getUserWithEmail(String email);

    @Query("SELECT * FROM user where email = :email and password = :psw")
    UserDomain getUserWithEmailAndPsw(String email,String psw);

}