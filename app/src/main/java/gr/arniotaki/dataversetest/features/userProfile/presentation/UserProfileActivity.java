package gr.arniotaki.dataversetest.features.userProfile.presentation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.features.login.presentation.ui.LoginActivity;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

public class UserProfileActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM = "mItem";

    @BindView(R.id.fullnameTV)
    TextView fullnameTV;
    @BindView(R.id.emailTV)
    TextView emailTV;
    @BindView(R.id.phoneTV)
    TextView phoneTV;
    @BindView(R.id.companyTV)
    TextView companyTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);
        UserDomain currentUser = getItem(getIntent());

        fullnameTV.setText(currentUser.getFullname());
        emailTV.setText(currentUser.getEmail());
        phoneTV.setText(currentUser.getPhone());
        companyTV.setText(currentUser.getCompany());
    }

    public static void setItem(@NonNull Intent intent, @Nullable UserDomain mItem) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_ITEM, mItem);
        intent.putExtras(bundle);
    }

    @Nullable
    public static UserDomain getItem(@NonNull Intent intent) {
        return (UserDomain) intent.getSerializableExtra(EXTRA_ITEM);
    }

    @OnClick(R.id.logout_btn)
    public void logoutClicked(){
        Toast.makeText(this,getString(R.string.successfull_logout),Toast.LENGTH_LONG).show();
        Intent i = new Intent(UserProfileActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }
}