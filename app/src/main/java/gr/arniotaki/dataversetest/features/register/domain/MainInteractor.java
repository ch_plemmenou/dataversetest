package gr.arniotaki.dataversetest.features.register.domain;

import android.content.Context;

public interface MainInteractor {

    void register(OnRegisterFinishListener listener, String username, String password, String phone, String email, String company, Context ctx);

    interface OnRegisterFinishListener {

        void onSuccess(UserDomain user);

        void onError(String message);

    }

}
