package gr.arniotaki.dataversetest.features.login.data;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.base.MyDatabase;
import gr.arniotaki.dataversetest.features.login.domain.LoginInteractor;
import gr.arniotaki.dataversetest.features.register.data.UserDao;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public void login(OnLoginFinishListener listener, String password, String email, Context ctx) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //Get user from db
                final UserDao dao = MyDatabase.getDatabase(ctx).userDao();
                //Check if user with the same email exists in db
                final UserDomain existingUser = dao.getUserWithEmailAndPsw(email, password);
                if (existingUser != null) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onSuccess(existingUser);
                        }
                    });
                }else {
                    //otherwise send an error message
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onError(ctx.getResources().getString(R.string.user_does_not_exist));
                        }
                    });
                }
            }
        });
    }
}
