package gr.arniotaki.dataversetest.features.register.domain;

public interface MainView {

    void userRegistered(boolean success,String failureMessage, UserDomain user);

}
