package gr.arniotaki.dataversetest.features.register.data;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.base.MyDatabase;
import gr.arniotaki.dataversetest.features.register.domain.MainInteractor;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

public class MainInteractorImpl implements MainInteractor {

    @Override
    public void register(OnRegisterFinishListener listener, String username, String password, String phone, String email, String company, Context ctx) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //Get user from db
                final UserDao dao = MyDatabase.getDatabase(ctx).userDao();
                final UserDomain user = new UserDomain(username,email,password,phone,company);
                //Check if user with the same email exists in db
                UserDomain existingUser = dao.getUserWithEmail(email);
                if (existingUser == null) {
                    //if not insert new user in db and inform with success
                    long insertedUserID = dao.insertUser(user);
                    if (insertedUserID > 0 ) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onSuccess(user);
                            }
                        });
                    }else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(ctx.getResources().getString(R.string.register_failed));
                            }
                        });
                    }
                }else {
                    //otherwise send an error message
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onError(ctx.getResources().getString(R.string.email_already_exists));
                        }
                    });
                }
            }
        });
    }

}
