package gr.arniotaki.dataversetest.features.login.domain;

import android.content.Context;

public interface LoginPresenter {

    boolean checkFields(String password, String email, Context ctx);
    void login(String password, String email, Context ctx);
}
