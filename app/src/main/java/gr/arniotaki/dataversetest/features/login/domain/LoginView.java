package gr.arniotaki.dataversetest.features.login.domain;

import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

public interface LoginView {

    void userLoggedIn(boolean success,String failureMessage, UserDomain user);

}
