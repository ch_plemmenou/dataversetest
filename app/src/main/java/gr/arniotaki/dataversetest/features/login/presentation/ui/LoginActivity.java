package gr.arniotaki.dataversetest.features.login.presentation.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.features.login.domain.LoginPresenter;
import gr.arniotaki.dataversetest.features.login.domain.LoginView;
import gr.arniotaki.dataversetest.features.register.domain.MainPresenter;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;
import gr.arniotaki.dataversetest.features.register.presentation.MainActivity;
import gr.arniotaki.dataversetest.features.register.presentation.MainPresenterImpl;
import gr.arniotaki.dataversetest.features.userProfile.presentation.UserProfileActivity;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.emailET)
    EditText email;
    @BindView(R.id.passwordET)
    EditText password;

    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        this.loginPresenter = new LoginPresenterImpl(this);
    }

    @OnClick(R.id.login_login_btn)
    public void login(View view) {

        String psw = password.getText().toString();
        String theEmail = email.getText().toString();

        boolean fieldsOk = loginPresenter.checkFields(psw, theEmail,this);
        if (fieldsOk) loginPresenter.login(psw, theEmail,this);
    }

    @Override
    public void userLoggedIn(boolean success, String failureMessage, UserDomain user) {
        if (success){
            Intent i = new Intent(LoginActivity.this, UserProfileActivity.class);
            UserProfileActivity.setItem(i, user);
            startActivity(i);
            finish();
        }else{
            Toast.makeText(this,failureMessage,Toast.LENGTH_LONG).show();
        }
    }
}