package gr.arniotaki.dataversetest.features.register.domain;

import android.content.Context;

public interface MainPresenter {

    boolean checkFields(String username, String password, String phone, String email, String repassword, String company, Context ctx);
    void registerUser(String username, String password, String phone, String email, String company, Context ctx);

}
