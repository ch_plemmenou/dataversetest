package gr.arniotaki.dataversetest.features.register.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.arniotaki.dataversetest.R;
import gr.arniotaki.dataversetest.features.register.domain.MainPresenter;
import gr.arniotaki.dataversetest.features.register.domain.MainView;
import gr.arniotaki.dataversetest.features.register.domain.UserDomain;
import gr.arniotaki.dataversetest.features.userProfile.presentation.UserProfileActivity;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.username_edittext)
    EditText username;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.company)
    EditText company;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.retype_password)
    EditText rePassword;

    @BindView(R.id.login_btn)
    Button mLoginButton;

    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        mainPresenter = new MainPresenterImpl(this);

        username.setText("Christina Plemmenou");
        password.setText("aeap09101989!");
        email.setText("test21@test.com");
        phone.setText("2152151230");
        company.setText("pavla");
        rePassword.setText("aeap09101989!");
    }

    @OnClick(R.id.login_btn)
    public void register(View view) {

        String usrname = username.getText().toString();
        String psw = password.getText().toString();
        String theEmail = email.getText().toString();
        String thePhone = phone.getText().toString();
        String theCompany = company.getText().toString();
        String repsw = rePassword.getText().toString();

        boolean fieldsOk = mainPresenter.checkFields(usrname,psw, thePhone,theEmail,repsw,theCompany,this);
        Timber.i("Fields are : "+fieldsOk);
        if (fieldsOk) mainPresenter.registerUser(usrname,psw,thePhone,theEmail,theCompany,this);
    }

    @Override
    public void userRegistered(boolean success, String failureMessage, UserDomain user) {
        if (success){
            Toast.makeText(this,getString(R.string.register_success),Toast.LENGTH_LONG).show();
            //go to user info activity
            Intent i = new Intent(MainActivity.this, UserProfileActivity.class);
            UserProfileActivity.setItem(i, user);
            startActivity(i);
            finish();
        }else{
            Toast.makeText(this,failureMessage,Toast.LENGTH_LONG).show();
        }
    }
}