package gr.arniotaki.dataversetest.features.login.domain;

import android.content.Context;

import gr.arniotaki.dataversetest.features.register.domain.UserDomain;

public interface LoginInteractor {

    void login(OnLoginFinishListener listener, String password, String email, Context ctx);

    interface OnLoginFinishListener {

        void onSuccess(UserDomain user);

        void onError(String message);

    }

}
