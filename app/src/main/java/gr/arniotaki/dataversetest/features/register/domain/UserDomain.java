package gr.arniotaki.dataversetest.features.register.domain;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "user", indices = {@Index(value = {"email"},
        unique = true)})
public class UserDomain implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public int userId;
    public String fullname;
    public String password;
    public String email;
    public String phone;
    public String company;



    public UserDomain(@NonNull String fullname, String email, String password, String phone, String company) {
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.company = company;
        this.password = password;
    }

    @NonNull
    public String getFullname() {
        return fullname;
    }

    public void setFullname(@NonNull String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}

